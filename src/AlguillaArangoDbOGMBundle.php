<?php


namespace Alguilla\ArangoDbOGMBundle;

use Alguilla\ArangoDbOGMBundle\DependencyInjection\AlguillaArangoDbOGMExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AlguillaArangoDbOGMBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        if (null === $this->extension) {
            $this->extension = new AlguillaArangoDbOGMExtension();
        }

        return $this->extension;
    }
}
