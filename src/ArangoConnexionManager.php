<?php


namespace Alguilla\ArangoDbOGMBundle;

use Alguilla\ArangoDbOGMBundle\Bridge\ApiPlatform\Paginator;
use ApiPlatform\Core\DataProvider\ArrayPaginator;
use ArangoDBClient\CollectionHandler as ArangoCollectionHandler;
use ArangoDBClient\Connection as ArangoConnection;
use ArangoDBClient\ConnectionOptions as ArangoConnectionOptions;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\EdgeHandler;
use ArangoDBClient\Statement;
use ArangoDBClient\UpdatePolicy;

class ArangoConnexionManager
{
    const RESULTS_PER_PAGE = 30;

    private $option_database;

    private $option_endpoint;

    private $option_auth_type;

    private $option_auth_user;

    private $option_auth_passwd;

    private $option_connection;

    private $option_timeout;

    private $option_reconnect;

    private $option_create;

    private $option_update_policy;

    public function __construct(
        $option_database = 'test',
        $option_endpoint = 'tcp://arangodb:8529',
        $option_auth_user = 'root',
        $option_auth_passwd = 'rootpassword',
        $option_auth_type = 'Basic',
        $option_connection = 'Keep-Alive',
        $option_timeout = 3,
        $option_reconnect = true,
        $option_create = true,
        $option_update_policy = UpdatePolicy::LAST)
    {
        $this->option_database = $option_database;
        $this->option_endpoint = $option_endpoint;
        $this->option_auth_type = $option_auth_type;
        $this->option_auth_user = $option_auth_user;
        $this->option_auth_passwd = $option_auth_passwd;
        $this->option_connection = $option_connection;
        $this->option_timeout = $option_timeout;
        $this->option_reconnect = $option_reconnect;
        $this->option_create = $option_create;
        $this->option_update_policy = $option_update_policy;
    }

    public function getConnexion()
    {
        $connectionOptions = [
            // database name
            ArangoConnectionOptions::OPTION_DATABASE => $this->option_database,
            // server endpoint to connect to
            ArangoConnectionOptions::OPTION_ENDPOINT => $this->option_endpoint,
            // authorization type to use (currently supported: 'Basic')
            ArangoConnectionOptions::OPTION_AUTH_TYPE => $this->option_auth_type,
            // user for basic authorization
            ArangoConnectionOptions::OPTION_AUTH_USER => $this->option_auth_user,
            // password for basic authorization
            ArangoConnectionOptions::OPTION_AUTH_PASSWD => $this->option_auth_passwd,
            // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
            ArangoConnectionOptions::OPTION_CONNECTION => $this->option_connection,
            // connect timeout in seconds
            ArangoConnectionOptions::OPTION_TIMEOUT => $this->option_timeout,
            // whether or not to reconnect when a keep-alive connection has timed out on server
            ArangoConnectionOptions::OPTION_RECONNECT => $this->option_reconnect,
            // optionally create new collections when inserting documents
            ArangoConnectionOptions::OPTION_CREATE => $this->option_create,
            // optionally create new collections when inserting documents
            ArangoConnectionOptions::OPTION_UPDATE_POLICY => $this->option_update_policy,
        ];


        return new ArangoConnection($connectionOptions);
    }

    public function getCollectionHandler($db = null)
    {
        $cnx = $db ? $this->getConnexion($db) : $this->getConnexion();
        return new ArangoCollectionHandler($cnx);
    }

    public function getDocumentHandler()
    {
        $cnx = $this->getConnexion();
        return new DocumentHandler($cnx);
    }

    public function getEdgeHandler()
    {
        $cnx = $this->getConnexion();

        return new EdgeHandler($cnx);
    }

    public function executeQuery($query, array $params = null): array
    {
        $statement = new Statement(
            $this->getConnexion(),
            array(
                "query" => $query,
                "bindVars" => $params,
                "count" => true,
                "batchSize" => 1000,
                "sanitize" => true
            )
        );
        $cursor = $statement->execute();

        $resultingDocuments = [];
        foreach ($cursor as $key => $value) {
            $resultingDocuments[$key] = $value;
        }

        return $resultingDocuments;
    }

    public function getPaginator($results, $page = 1)
    {
        if ($page) {
            $offset = $page > 1 ? $page * self::RESULTS_PER_PAGE - self::RESULTS_PER_PAGE : 0;
        } else {
            $offset = 0;
        }

        return new ArrayPaginator($results, $offset, self::RESULTS_PER_PAGE);
    }
}
