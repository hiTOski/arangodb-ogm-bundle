<?php


namespace Alguilla\ArangoDbOGMBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('alguilla_arango_db_ogm');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('database')->defaultValue('_system')->info('Your default database name')->end()
                ->scalarNode('endpoint')->defaultValue('tcp://127.0.0.1:8529')
                    ->info('Endpoint to connect to database (default: tcp://127.0.0.1:8529')->end()
                ->scalarNode('user')->defaultValue('root')->info('Database user name (default: root)')->end()
                ->scalarNode('password')->defaultValue('rootpassword')->info('Database user password (default: rootpassword)')->end()
            ->end();

        return $treeBuilder;
    }
}
