<?php

namespace Alguilla\ArangoDbOGMBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class AlguillaArangoDbOGMExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');

        $configuration = $this->getConfiguration($configs, $container);

        $config = $this->processConfiguration($configuration, $configs);

        $definition = $container->getDefinition('alguilla_arangodb_ogm.arangodb_ogm');
        $definition->setArgument(0, $config['database']);
        $definition->setArgument(1, $config['endpoint']);
        $definition->setArgument(2, $config['user']);
        $definition->setArgument(3, $config['password']);
    }
}

