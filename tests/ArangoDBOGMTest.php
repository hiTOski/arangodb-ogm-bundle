<?php


namespace Alguilla\ArangoDBOGMBundle\Tests;


use Alguilla\ArangoDbOGMBundle\ArangoConnexionManager;
use ArangoDBClient\Collection;
use ArangoDBClient\Database;
use PHPUnit\Framework\TestCase;

class ArangoDBOGMTest extends TestCase
{
    public function testGetConnexion()
    {
        $databaseTest = $this->getTestConnexion();

        $collectionHandler = $databaseTest->getCollectionHandler();

        if ($collectionHandler->has('users')) {
            $collectionHandler->drop('users');
        }

        // create a new collection

        $userCollection = new Collection();
        $userCollection->setName('users');
        $collectionHandler->create($userCollection);
        // check if the collection exists
        $this->assertTrue($collectionHandler->has('users'));
        $databaseTest->executeQuery("INSERT {name: 'user1'} INTO users");

        $result = $databaseTest->executeQuery("FOR u IN users RETURN u");

        $this->assertIsArray($result);
        $this->assertEquals('user1', $result[0]->get('name'));
    }

    private function getTestConnexion() {
        $arangoDbManager = new ArangoConnexionManager('_system');

        $database = new Database();
        if (in_array('testDatabase', $database->listDatabases($arangoDbManager->getConnexion())['result'])) {
            $database->delete($arangoDbManager->getConnexion(), 'testDatabase');
        }

        $database->create($arangoDbManager->getConnexion(), 'testDatabase');

        return new ArangoConnexionManager('testDatabase');
    }
}
