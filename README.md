# ArangoDBogm

ArangoDB OGM Bundle

# What is it ?

It's a bundle aiming to mimic the behaviour of symfony ORM & ODM for ArangoDB. Work in progress

# How to use it ? 

Just copy/past this in your project directory :
```console
composer require alguilla/arangodb-ogm-bundle --dev
```
